# Ozan Test Project

## Kurulum

- Proje indirildikten sonra SPM (Swift Package Manager) çalıştırmak gerekiyor..

## Mimari

Projede Viper mimarisini kullandım.
VIPER merkezine presenter’ı alan, servis işlemlerini, ekran geçişlerini ve kullanıcının gördüğü arayüzü ayrı katmanlara ayıran bir mimaridir. Kodu test edilebilir hale getirirken düzenli yapısı sayesinde ek geliştirmelere açıktır. VIPER  5 katmandan oluşur. Bu katmanların birleşimi bir ekranı temsil eder. Aynı zamanda her bir katman birer sınıftır.
V → View: (UIViewController, UITableController vb.)
I → Interactor (Servis ile haberleşmenin yapıldığı katman)
P → Presenter (View, Router ve Interactor arasındaki bağlantıyı sağlayan katman)
E → Entity (Model sınıfı)
R → Router veya Wireframe(Ekranlar arası geçişin kontrol edildiği katman)

Entity hariç her bir katmanın protocol’ü bulunur. Bu protocol’ler ile katmanlar arası iletişim kurulur.
Kullanıcının gezindiği sayfaları ve sildiği sayfaları UserReactionHelper classından yönetimi sağlıyoruz. Bu class CacheManager classının protokollerini kullanıyor. 
CacheManager classında verileri cachlemek için UserDefault kullandım.

Image download işlemleri ve image cachleme için SDWebImage kullandım. SDWebImage kütüphanesini projeye Swift Package Dependencies ile ekledim.

## Network

Network bağlantısı için URLSession kullandım. ConnectionManager class execute fonksiyonuna URL göndererek response callback olarak alıyoruz. 
URL ise Api classında tanımladım. URL i bu classtan yönetmek işleri kolaylaştırıyor.
