//
//  UITableViewExtension.swift
//  OzanTestProject
//
//  Created by Semih EKIZ on 7.10.2021.
//  Copyright (c) 2021 EKIZ. All rights reserved.
//

import UIKit

protocol UITableViewCellDequeueable {

}

extension UITableViewCellDequeueable where Self: UITableViewCell {

    static func dequeue(from tableView: UITableView, reuseIdentifier: String! = nil) -> Self {
        let reuseIdentifier = reuseIdentifier ?? msReuseIdentifier

        guard let cell = self.optDequeue(from: tableView, reuseIdentifier: reuseIdentifier) else {
            register(to: tableView, reuseIdentifier: reuseIdentifier)
            return optDequeue(from: tableView, reuseIdentifier: reuseIdentifier)
        }

        return cell
    }

    static func optDequeue(from tableView: UITableView, reuseIdentifier: String! = nil) -> Self! {
        let reuseIdentifier = reuseIdentifier ?? msReuseIdentifier

        return tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? Self
    }

    static func register(to tableView: UITableView, reuseIdentifier: String! = nil) {
        let reuseIdentifier = reuseIdentifier ?? msReuseIdentifier

        if let nib = nib() {
            tableView.register(nib, forCellReuseIdentifier: reuseIdentifier)
        } else {
            tableView.register(self, forCellReuseIdentifier: reuseIdentifier)
        }

    }

    static func dequeueClass(from tableView: UITableView, reuseIdentifier: String! = nil) -> Self {
        let reuseIdentifier = reuseIdentifier ?? msReuseIdentifier

        guard let cell = self.optDequeue(from: tableView, reuseIdentifier: reuseIdentifier) else {
            registerClass(to: tableView, reuseIdentifier: reuseIdentifier)
            return optDequeueClass(from: tableView, reuseIdentifier: reuseIdentifier)
        }

        return cell
    }

    static func optDequeueClass(from tableView: UITableView, reuseIdentifier: String! = nil) -> Self! {
        let reuseIdentifier = reuseIdentifier ?? msReuseIdentifier

        return tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? Self
    }

    static func registerClass(to tableView: UITableView, reuseIdentifier: String! = nil) {
        let reuseIdentifier = reuseIdentifier ?? msReuseIdentifier

        tableView.register(self, forCellReuseIdentifier: reuseIdentifier)

    }
}

extension UITableViewCell: UITableViewCellDequeueable {

    @objc class var msReuseIdentifier: String {
        return String(describing: self)
    }

    @objc class func nib() -> UINib! {
        let className = String(describing: self)

        return UINib(nibName: className, bundle: nil)
    }

}

protocol UITableViewHeaderFooterViewDequeueable {

}

extension UITableViewHeaderFooterViewDequeueable where Self: UITableViewHeaderFooterView {

    static func dequeue(from tableView: UITableView) -> Self {

        guard let view = self.optDequeue(from: tableView) else {
            register(to: tableView)

            return optDequeue(from: tableView)
        }

        return view
    }

    static func optDequeue(from tableView: UITableView) -> Self! {
        return tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseIdentifier) as? Self
    }

    static  func register(to tableView: UITableView) {

        if let nib = nib() {
            tableView.register(nib, forHeaderFooterViewReuseIdentifier: reuseIdentifier)
        } else {
            tableView.register(self, forHeaderFooterViewReuseIdentifier: reuseIdentifier)
        }

    }
}

extension UITableViewHeaderFooterView: UITableViewHeaderFooterViewDequeueable {

    @objc class var reuseIdentifier: String {
        return String(describing: self)
    }

    @objc class func nib() -> UINib! {
        let className = String(describing: self)

        return UINib(nibName: className, bundle: nil)
    }
}

extension UITableView {
    func removeEmptyCell() {
        self.tableFooterView = UIView()
    }
}
