//
//  SceneDelegate.swift
//  OzanTestProject
//
//  Created by Semih EKIZ on 7.10.2021.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var nav: UINavigationController?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            nav = UINavigationController()
            nav?.setRootWireframe(ItemListWireframe())
            window.rootViewController = nav!
            self.window = window
            window.makeKeyAndVisible()
        }
    }
}
