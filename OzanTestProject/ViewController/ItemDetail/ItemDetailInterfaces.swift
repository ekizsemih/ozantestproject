//
//  ItemDetailInterfaces.swift
//  OzanTestProject
//
//  Created by Semih EKİZ on 7.10.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

protocol ItemDetailWireframeInterface: WireframeInterface {
    func showDeleteMessage(_ model: ItemModel)
}

protocol ItemDetailViewInterface: ViewInterface {
    func setViewTitle(_ title: String?)
    func reloadData()
}

protocol ItemDetailPresenterInterface: PresenterInterface {
    func showDeleteMessage()
    
    func numberOfSections() -> Int
    func numberOrItems(in section: Int) -> Int
    func item() -> ItemModel
}

protocol ItemDetailInteractorInterface: InteractorInterface {
}
