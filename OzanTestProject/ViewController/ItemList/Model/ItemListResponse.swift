//
//  ItemListResponse.swift
//  OzanTestProject
//
//  Created by Semih EKIZ on 7.10.2021.
//  Copyright (c) 2021 EKIZ. All rights reserved.
//

import Foundation

struct ItemListResponse : Codable {
    
	let resultCount : Int?
	let results : [ItemModel]?

	enum CodingKeys: String, CodingKey {
        case resultCount = "resultCount"
		case results = "results"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		resultCount = try values.decodeIfPresent(Int.self, forKey: .resultCount)
		results = try values.decodeIfPresent([ItemModel].self, forKey: .results)
	}
}
