//
//  ItemDetailCell.swift
//  OzanTestProject
//
//  Created by Semih EKİZ on 7.10.2021.
//

import UIKit

class ItemDetailCell: UITableViewCell {
   
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    func bind(_ model: ItemModel) {
        
        titleLabel.text = model.collectionName
        subTitleLabel.text = model.trackName
        
        if let url = URL(string: model.artworkUrl60!) {
            coverImageView.sd_setImage(with: url, completed: nil)
        }
    }
    
}
