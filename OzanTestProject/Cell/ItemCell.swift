//
//  ItemCell.swift
//  OzanTestProject
//
//  Created by Semih EKİZ on 7.10.2021.
//

import UIKit
import SDWebImage

class ItemCell: UICollectionViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    func bind(_ model: ItemModel) {
        titleLabel.text = model.artistName
        containerView.backgroundColor = model.isSelected ?? false ? UIColor.lightGray.withAlphaComponent(0.5) : UIColor.white
        if let url = URL(string: model.artworkUrl60!) {
            coverImageView.sd_setImage(with: url, completed: nil)
        }
    }
}


