//
//  CacheManager.swift
//  OzanTestProject
//
//  Created by Semih EKİZ on 8.10.2021.
//

import UIKit

class CacheManager: NSObject {

    static let sharedInstance = CacheManager()

    // MARK: String
    func writeStringToUserDefaults(string: String, key: String) {
        UserDefaults.standard.setValue(string, forKey: key)
        UserDefaults.standard.synchronize()
    }

    func readStringFromUserDefaults(key: String) -> String {
        return UserDefaults.standard.string(forKey: key) ?? ""
    }

    // MARK: Integer
    func writeIntegerToUserDefaults(integer: NSInteger, key: String) {
        UserDefaults.standard.setValue(integer, forKey: key)
        UserDefaults.standard.synchronize()
    }

    func readIntegerFromUserDefaults(key: String) -> Int {
        return UserDefaults.standard.integer(forKey: key)
    }

    // MARK: Bool
    func writeBoolToUserDefaults(bool: Bool, key: String) {
        UserDefaults.standard.setValue(bool, forKey: key)
        UserDefaults.standard.synchronize()
    }

    func readBoolFromUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.bool(forKey: key)
    }

    // MARK: Array
    func writeArrayToUserDefaults(array: [Any], key: String) {
        UserDefaults.standard.setValue(array, forKey: key)
        UserDefaults.resetStandardUserDefaults()
    }

    func readArrayFromUserDefaults(key: String) -> [Any]? {
        return UserDefaults.standard.array(forKey: key)
    }

    // MARK: Remove
    func removeAllKeys() {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
    }
}
