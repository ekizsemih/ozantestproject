//
//  UserReactionHelper.swift
//  OzanTestProject
//
//  Created by Semih EKİZ on 8.10.2021.
//

import Foundation

protocol UserReactionProcol {
    func getSelectedItem() -> [Int]
    func getDeletedItem() -> [Int]
    func addSelectedItem(_ id: Int)
    func addDeletedItem(_ id: Int)
    func listUpdate(_ oldList: [ItemModel])-> [ItemModel]
}

private(set) var kSelected = "SelectedCacheList"
private(set) var kDeleted = "DeletedCacheList"

final class UserReactionHelper: UserReactionProcol {
    
    let cacheManager = CacheManager()
    
    func getSelectedItem() -> [Int] {
        return cacheManager.readArrayFromUserDefaults(key: kSelected) as? [Int] ?? []
    }
    
    func addSelectedItem(_ id: Int) {
        
        var list = cacheManager.readArrayFromUserDefaults(key: kSelected) as? [Int] ?? []
        
        if list.filter({ $0 == id }).isEmpty {
            list.append(id)
        }
        
        cacheManager.writeArrayToUserDefaults(array: list, key: kSelected)
    }
    
    func addDeletedItem(_ id: Int) {
        var deletedList = cacheManager.readArrayFromUserDefaults(key: kDeleted) as? [Int] ?? []
        deletedList.append(id)
        cacheManager.writeArrayToUserDefaults(array: deletedList, key: kDeleted)
    }
    
    func getDeletedItem() -> [Int] {
        return cacheManager.readArrayFromUserDefaults(key: kDeleted) as? [Int] ?? []
    }
    
    
    func listUpdate(_ oldList: [ItemModel])-> [ItemModel] {
        
        var updateList = oldList
        let deletedIdList = cacheManager.readArrayFromUserDefaults(key: kDeleted) as? [Int] ?? []
        let selectedIdList = cacheManager.readArrayFromUserDefaults(key: kSelected) as? [Int] ?? []
        
        // MARK: remove deleted items
        deletedIdList.forEach { deleteId in
            for (index,model) in updateList.enumerated() {
                if model.trackId == deleteId {
                    updateList.remove(at: index)
                }
            }
        }
        
        // MARK: update inspected items
        selectedIdList.forEach { selectedId in
            for (index,model) in updateList.enumerated() {
                if model.trackId == selectedId {
                    var tmodel = model
                    tmodel.isSelected = true
                    updateList[index] = tmodel
                }
            }
        }
        
        return updateList
    }
}
