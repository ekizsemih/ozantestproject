//
//  Api.swift
//  OzanTestProject
//
//  Created by Semih EKİZ on 8.10.2021.
//

import Foundation

struct Api {
    static let url =  "https://itunes.apple.com/"
    static let listUrl = url + "search?term=%@&limit=%@"
}
