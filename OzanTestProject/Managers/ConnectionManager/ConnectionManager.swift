//
//  ConnectionManager.swift
//  OzanTestProject
//
//  Created by Semih EKİZ on 7.10.2021.
//

import Foundation

enum NetworkError: Error, Equatable {
    case serverError(statusCode: Int)
    case noData
    case dataDecodingError
    case invalidUrl
}

final class ConnectionManager {
    
    private(set) var session: URLSession
    private var queue: DispatchQueue
    
    public init(queue: DispatchQueue = .main, session: URLSession = .shared) {
        self.queue = queue
        self.session = session
    }
    
    func execute(url: String, completion: @escaping (Result<Data, Error>) -> Void) {
        
        print("URL: " + url)
        
        guard let _url = URL(string: url) else {
            completion(.failure(NetworkError.invalidUrl))
            return
        }
        
        let task = session.dataTask(with: _url) { data, response, error in
            self.queue.async { [self] in
                
                printResponse(data: data!)
                
                if error != nil {
                    completion(.failure(NetworkError.invalidUrl))
                    return
                }
                
                if let response = response as? HTTPURLResponse, !(200...299).contains(response.statusCode) {
                    completion(.failure(NetworkError.serverError(statusCode: response.statusCode)))
                    return
                }
                
                if let data = data, !data.isEmpty {
                    completion(.success(data))
                }
                else {
                    completion(.failure(NetworkError.noData))
                }
            }
        }
        
        task.resume()
    }
    
    func printResponse(data: Data) {
        do {
            // make sure this JSON is in the format we expect
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                // try to read out a string array
                print("\nRESPONSE: ",json)
            }
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
    }
}
